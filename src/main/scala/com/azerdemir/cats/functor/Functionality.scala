package com.azerdemir.cats.functor

import scala.util.{Failure, Success, Try}

object Functionality extends App {

  /**
    * Functor is a type class that abstracts over type constructors that can be map'ed over.
    * Examples of such type constructors are <code>List</code>, <code>Option</code>, and <code>Future</code>.
    */

  trait Functor[F[_]] {
    def map[A, B](fa: F[A])(f: A => B): F[B]
  }

  // Example implementation for Option
  implicit val optionFunctor: Functor[Option] = new Functor[Option] {

    def map[A, B](fa: Option[A])(f: A => B): Option[B] = fa match {
      case None    => None
      case Some(a) => Some(f(a))
    }

  }

  /**
    * A Functor instance must obey two laws:
    * - Composition: Mapping with f and then again with g is the same as mapping once with the composition of f and g.
    *   fa.map(f).map(g) = fa.map(f.andThen(g))
    * - Identity: Mapping with the identity function is a no-op.
    *   fa.map(x => x) = fa
    */
  val maybeAgentId               = Some(42)
  val maybeNone                  = None
  val agentIdNameMapping         = Map(42 -> "Agent Smith")
  val agentMapper: Int => String = agentId => agentIdNameMapping.getOrElse(agentId, "Not Found")

  val maybeAgentName = optionFunctor.map(maybeAgentId)(agentMapper)
  val maybeNobody    = optionFunctor.map(maybeNone)(agentMapper)

  Try {
    assert(maybeAgentName.contains("Agent Smith"))
    assert(maybeNobody.isEmpty)
  } match {
    case Success(_) => println("Option functor works properly.. \uD83C\uDF89")
    case Failure(_) => println("Not working!!")
  }

  /**
  * Another way of viewing a Functor[F] is that F allows the lifting of a pure function A => B into the effectful function F[A] => F[B].
  * We can see this if we re-order the map signature above.
  */

  trait FunctorHasLift[F[_]] {
    def map[A, B](fa: F[A])(f: A => B): F[B]

    def lift[A, B](f: A => B): F[A] => F[B] =
      fa => map(fa)(f)
  }

  /**
    * The F in Functor is often referred to as an "effect" or "computational context".
    * Different effects will abstract away different behaviors with respect to fundamental functions like map.
    * For instance, Option's effect abstracts away potentially missing values, where map applies the function only in the Some case
    * but otherwise threads the None through.
    *
    * Taking this view, we can view Functor as the ability to work with a single effect -
    * we can apply a pure function to a single effectful value without needing to "leave" the effect.
    */

}
