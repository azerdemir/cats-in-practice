package com.azerdemir.cats.functor

import cats.data.Nested
import cats.Functor
import cats.implicits._

import scala.util.{Failure, Success, Try}

object Composition extends App {

  /**
    * If you’ve ever found yourself working with nested data types such as
    * <code>Option[List[A]]</code> or <code>List[Either[String, Future[A]]]</code> and tried to map over it,
    * you’ve most likely found yourself doing something like _.map(_.map(_.map(f))).
    * As it turns out, Functors compose, which means if F and G have Functor instances, then so does <code>F[G[_]]</code>.
    *
    * Such composition can be achieved via the Functor#compose method.
    */
  val listOption      = List(Some(1), None, Some(2))
  val mapedListOption = Functor[List].compose[Option].map(listOption)(_ + 1)

  Try(assert(mapedListOption == List(Some(2), None, Some(3)))) match {
    case Success(_) => println("Functor composition works properly.. \uD83C\uDF89")
    case Failure(_) => println("Functor composition not working!!")
  }

  /**
    * This approach will allow us to use composition without wrapping the value in question,
    * but can introduce complications in more complex use cases.
    * For example, if we need to call another function which requires a Functor and we want to use the composed Functor,
    * we would have to explicitly pass in the composed instance during the function call or create a local implicit.
    */
  /*
  def needsFunctor[F[_]: Functor, A](fa: F[A]): F[Unit] = Functor[F].map(fa)(_ => ())

  def foo: List[Option[Unit]] = {
    type ListOption[A] = List[Option[A]]
    val listOptionFunctor: Functor[ListOption] = Functor[List].compose[Option]
    needsFunctor[ListOption, Int](listOption)(listOptionFunctor)
  }
   */

  /**
    * We can make this nicer at the cost of boxing with the Nested data type.
    */
  val nested: Nested[List, Option, Int] = Nested(listOption)

  Try(assert(nested.map(_ + 1).value == List(Some(2), None, Some(3)))) match {
    case Success(_) => println("Nested works properly.. \uD83C\uDF89")
    case Failure(_) => println("Nested data type not working!!")
  }

}
