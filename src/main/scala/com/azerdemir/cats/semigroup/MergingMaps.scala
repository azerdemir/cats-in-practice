package com.azerdemir.cats.semigroup

import cats.implicits._
import cats.kernel.Semigroup

import scala.util.{Failure, Success, Try}

object MergingMaps extends App {

  val xm1 = Map('a' -> 1, 'b' -> 2)
  val xm2 = Map('b' -> 3, 'c' -> 4)
  val x   = mergeMaps(xm1, xm2)

  Try(assert(x == Map('b' -> 5, 'c' -> 4, 'a' -> 1))) match {
    case Success(_) => println("Merging maps successful for Int as V type.")
    case Failure(_) => println("Merging maps failed for Int as V type.")
  }

  val ym1 = Map(1 -> List("hello"))
  val ym2 = Map(2 -> List("cats"), 1 -> List("world"))
  val y   = mergeMaps(ym1, ym2)

  Try(assert(y == Map(2 -> List("cats"), 1 -> List("hello", "world")))) match {
    case Success(_) => println("Merging maps successful for List[String] as V type.")
    case Failure(_) => println("Merging maps failed for List[String] as V type.")
  }

  def optionCombine[A: Semigroup](a: A, opt: Option[A]): A =
    opt.map(a |+| _).getOrElse(a)

  def mergeMaps[K, V: Semigroup](lhs: Map[K, V], rhs: Map[K, V]): Map[K, V] =
    lhs.foldLeft(rhs) {
      case (acc, (k, v)) => acc.updated(k, optionCombine(v, acc.get(k)))
    }

}
