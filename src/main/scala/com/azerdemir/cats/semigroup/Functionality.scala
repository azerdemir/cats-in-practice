package com.azerdemir.cats.semigroup

import cats.Semigroup

import scala.util.{Failure, Success, Try}

object Functionality extends App {

  intAddition()
  longSubtraction()

  def intAddition(): Unit = {

    /**
      * If a type A can form a Semigroup it has an associative binary operation.
      * <b>Associativity</b> means the following equality must hold for any choice of x, y, and z.
      */
    implicit val intAdditionSemigroup: Semigroup[Int] = (x: Int, y: Int) => x + y

    //val Triple(x, y, z) = intTriple
    val x = 1
    val y = 2
    val z = 3

    val combined1: Int = Semigroup[Int].combine(x, Semigroup[Int].combine(y, z))
    val combined2: Int = Semigroup[Int].combine(Semigroup[Int].combine(x, y), z)

    assert(combined1 == combined2)

    /**
      * Infix syntax is also available for types that have a Semigroup instance.
      */
    //assert((x |+| (y |+| z)) == ((x |+| y) |+| z))

    println("Int addition semigroup works properly.. \uD83C\uDF89")

  }

  def longSubtraction(): Unit = {
    implicit val longSubtractionSemigroup: Semigroup[Long] = (x: Long, y: Long) => x - y

    //val Triple(x, y, z) = longTriple
    val x = 1L
    val y = 2L
    val z = 3L

    val combined1: Long = Semigroup[Long].combine(x, Semigroup[Long].combine(y, z))
    val combined2: Long = Semigroup[Long].combine(Semigroup[Long].combine(x, y), z)

    Try(assert(combined1 == combined2)) match {
      case Success(_) => println("Long subtraction semigroup works properly.. \uD83C\uDF89")
      case Failure(_) => println("Long subtraction is not associative!! \uD83D\uDE20")
    }
  }

  val intTriple  = Triple(1, 2, 3)
  val longTriple = Triple(1L, 2L, 3L)

  case class Triple[A](x: A, y: A, z: A)

}
