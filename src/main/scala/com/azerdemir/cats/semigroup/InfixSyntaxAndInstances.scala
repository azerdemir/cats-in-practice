package com.azerdemir.cats.semigroup

import cats.implicits._

object InfixSyntaxAndInstances extends App {

  intAddition()

  def intAddition(): Unit = {

    /**
      * Infix syntax is also available for types that have a Semigroup instance.
      */
    val combinedInt = 1 |+| 2 |+| 3
    val combinedStr = "Hello" |+| " world" |+| "!"

    println(s"Combined int value is $combinedInt.")
    println(s"Combined str value is $combinedStr.")
    println("Yaaayyy! \uD83C\uDF89")

  }

}
