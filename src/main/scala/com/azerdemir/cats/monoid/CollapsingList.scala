package com.azerdemir.cats.monoid

import cats.Monoid
import cats.implicits._

import scala.util.{Failure, Success, Try}

object CollapsingList extends App {

  /**
    * In the Semigroup section we had trouble writing a generic combineAll function
    * because we had nothing to give if the list was empty.
    * With Monoid we can return empty, and define a combineAll function.
    */
  Try {
    assert(6 == combineAll(List(1, 2, 3)))
    assert("hello world" == combineAll(List("hello", " ", "world")))
    assert(
      Map('b' -> 7, 'c' -> 5, 'a' -> 3) == combineAll(
        List(Map('a' -> 1), Map('a' -> 2, 'b' -> 3), Map('b' -> 4, 'c' -> 5))
      )
    )
  } match {
    case Success(_) => println("combineAll works for Monoids. \uD83C\uDF89")
    case Failure(_) => println("Noooo waaaayy!! Are you serious?")
  }

  /**
    * This function is provided in Cats as Monoid.combineAll.
    */

  def combineAll[A: Monoid](as: List[A]): A =
    as.foldLeft(Monoid[A].empty)(Monoid[A].combine)

}
