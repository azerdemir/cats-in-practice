package com.azerdemir.cats.monoid

import cats.Monoid

import scala.util.{Failure, Success, Try}

object Functionality extends App {

  /**
    * Monoid extends the power of Semigroup by providing an additional empty value.
    * This empty value should be an identity for the combine operation,
    * which means the following equalities hold for any choice of x.
    */

  implicit val intAdditionMonoid: Monoid[Int] = new Monoid[Int] {
    def empty: Int                   = 0
    def combine(x: Int, y: Int): Int = x + y
  }

  val x = 1

  Try {
    assert(x == Monoid[Int].combine(x, Monoid[Int].empty))
    assert(x == Monoid[Int].combine(Monoid[Int].empty, x))
  } match {
    case Success(_) => println("Int monoid works properly.. \uD83C\uDF89")
    case Failure(_) => println("Whaaaaatttt!! Houston we have a problem!!")
  }

}
