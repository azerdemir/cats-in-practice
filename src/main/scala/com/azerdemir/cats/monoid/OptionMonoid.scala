package com.azerdemir.cats.monoid

import cats.Semigroup
import cats.implicits._
import cats.Monoid
import scala.util.Failure
import scala.util.Success
import scala.util.Try

object OptionMonoid extends App {

  /**
    * There are some types that can form a Semigroup but not a Monoid.
    * For example, the following NonEmptyList type forms a semigroup through ++,
    * but has no corresponding identity element to form a monoid.
    */
  final case class NonEmptyList[A](head: A, tail: List[A]) {
    def ++(other: NonEmptyList[A]): NonEmptyList[A] = NonEmptyList(head, tail ++ other.toList)
    def toList: List[A]                             = head :: tail
  }

  object NonEmptyList {

    implicit def nonEmptyListSemigroup[A]: Semigroup[NonEmptyList[A]] = new Semigroup[NonEmptyList[A]] {
      def combine(x: NonEmptyList[A], y: NonEmptyList[A]): NonEmptyList[A] = x ++ y
    }

  }

  /**
    * How then can we collapse a List[NonEmptyList[A]] ?
    * For such types that only have a Semigroup,
    * we can lift into Option to get a Monoid.
    */
  /*
  implicit def optionMonoid[A: Semigroup]: Monoid[Option[A]] = new Monoid[Option[A]] {
    def empty: Option[A] = None

    def combine(x: Option[A], y: Option[A]): Option[A] =
      x match {
        case None => y
        case Some(xv) =>
          y match {
            case None     => x
            case Some(yv) => Some(xv |+| yv)
          }
      }
  }
   */

  /**
    * This is the Monoid for Option: for any Semigroup[A],
    * there is a Monoid[Option[A]].
    */
  val list     = List(NonEmptyList(1, List(2, 3)), NonEmptyList(4, List(5, 6)))
  val lifted   = list.map(nel => Option(nel))
  val combined = Monoid.combineAll(lifted)

  Try(assert(combined == Some(NonEmptyList(1, List(2, 3, 4, 5, 6))))) match {
    case Success(_) => println("Option monoid for NonEmptyList semi group works properly.. \uD83C\uDF89")
    case Failure(_) => println("Int monoid works perfect..")
  }

  // This lifting and combining of Semigroups into Option is provided by Cats as Semigroup.combineAllOption.

}
