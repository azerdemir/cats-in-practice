name         := "cats-in-practice"
version      := "0.1"
scalaVersion := "2.13.2"

libraryDependencies += "org.typelevel" %% "cats-core" % "2.0.0"
